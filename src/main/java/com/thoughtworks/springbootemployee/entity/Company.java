package com.thoughtworks.springbootemployee.entity;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private Integer id;
    private String name;



    public static List<Company> initCompany(){
        List<Company> companies = new ArrayList<>();

        companies.add(new Company(1,"test"));
        companies.add(new Company(2,"test2"));
        companies.add(new Company(3,"test3"));
        companies.add(new Company(4,"test4"));
        companies.add(new Company(5,"test5"));
        companies.add(new Company(6,"test6"));

        return companies;
    }

    public Company(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
