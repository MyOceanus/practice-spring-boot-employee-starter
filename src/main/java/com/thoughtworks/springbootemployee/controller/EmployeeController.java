package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.EnumMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
public class EmployeeController {


    public static EmployeeRepository employeeRepository(){
       return new EmployeeRepository();
    }

    @GetMapping()
    public List<Employee> getAll(){
        return employeeRepository().getEmployees();
    }

    @GetMapping(value = "/{id}")
    public Employee getEmployeeById(@PathVariable Integer id){
       return employeeRepository().getEmployeeById(id);
    }

    @GetMapping(params = "gender")
    public List<Employee> getEmployeesByGender(@RequestParam String gender){
        return employeeRepository().getEmployeesByGender(gender);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addEmployee(@RequestBody Employee employee) {
        employeeRepository().addEmployee(employee);
    }

    @PutMapping("/{id}")
    public void updateEmployeeById(@PathVariable Integer id, @RequestBody Employee employee) {
        employeeRepository().updateEmployeeById(id,employee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployeeById(@PathVariable Integer id) {
        employeeRepository().deleteEmployeeById(id);
    }

    @GetMapping(params = {"page","size"})
    public List<Employee> getByPageAndSize(@RequestParam Integer page,@RequestParam Integer size){
            return employeeRepository().getByPageAndSize(page,size);
    }
}
