package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.CompanyRepository;
import com.thoughtworks.springbootemployee.service.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/companies")
public class CompanyController {


    public static CompanyRepository companyRepository(){
        return new CompanyRepository();
    }

    @GetMapping
    public List<Company> getAllCompany(){
        return CompanyRepository.companies;
    }

    @GetMapping("/{companyId}")
    public Company getCompanyById(@PathVariable Integer id){
        return companyRepository().getCompanyById(id);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Integer companyId){
        return companyRepository().getEmployeesByCompanyId(companyId);
    }


    @GetMapping(params = {"page","size"})
    public List<Company> getCompaniesByPage(@RequestParam Integer page,@RequestParam Integer size){
        return companyRepository().getCompaniesByPage(page,size);
    }

    @PostMapping()
    public void addCompanies(@RequestBody Company company){
       companyRepository().addCompanies(company);
    }


    @PutMapping("/{companyId}")
    public void updateCompanies(@PathVariable Integer companyId,@RequestBody Company company){
        companyRepository().updateCompanies(companyId,company);
    }


    @DeleteMapping("/{companyId}")
    public void deleteCompany(@PathVariable Integer companyId){
        companyRepository().deleteCompany(companyId);
    }

}
