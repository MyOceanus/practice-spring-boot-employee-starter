package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.List;
import java.util.stream.Collectors;

public class CompanyRepository {

    public static List<Company> companies = Company.initCompany();

    public List<Company> getAllCompany(){
        return companies;
    }

    public static List<Company> getCompanies() {
        return companies;
    }

    public Company getCompanyById(Integer id){
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst().orElse(null);
    }


    public List<Employee> getEmployeesByCompanyId( Integer companyId){
        return EmployeeRepository.employees.stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }



    public List<Company> getCompaniesByPage(Integer page,  Integer size){
        return companies.stream()
                .skip((page-1)*size)
                .limit(size)
                .collect(Collectors.toList());
    }


    public void addCompanies( Company company){
        company.setId(nextId());
        companies.add(company);
    }

    private  Integer nextId() {
        int currentId = companies.stream()
                .mapToInt(Company::getId)
                .max()
                .orElse(1);
        return ++currentId;
    }

    public void updateCompanies( Integer companyId, Company company){
        Company updateCompanies = companies.stream()
                .filter(companyItem -> companyItem.getId().equals(companyId))
                .findFirst()
                .orElse(null);

        updateCompanies.setName(company.getName() == null ? updateCompanies.getName() : company.getName());
    }


    public void deleteCompany( Integer companyId){
        Company deleteCompany = companies.stream()
                .filter(companyItem -> companyItem.getId().equals(companyId))
                .findFirst()
                .orElse(null);
        companies.remove(deleteCompany);

        EmployeeRepository.employees.remove(
                EmployeeRepository.employees.stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .findFirst()
                .orElse(null));
    }
}
