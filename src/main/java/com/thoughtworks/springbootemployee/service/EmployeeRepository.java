package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeRepository {

    public static List<Employee> employees = Employee.init();

    public List<Employee> getEmployees() {
        return employees;
    }

    public Employee getEmployeeById(Integer id){
        return employees.stream().
                filter(employee -> employee.getId().equals(id)).
                findFirst().orElse(null);
    }


    public List<Employee> getEmployeesByGender(String gender){
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }


    public void addEmployee(Employee employee) {
        employee.setId(nextId());
        employees.add(employee);
    }

    private  Integer nextId() {
        int currentId = employees.stream()
                .mapToInt(Employee::getId)
                .max()
                .orElse(1);
        return ++currentId;
    }

    public void updateEmployeeById(Integer id, Employee employee) {
        Employee updateEmployee = employees.stream()
                .filter(employeeItem -> employeeItem.getId().equals(id))
                .findFirst()
                .orElse(null);
        updateEmployee.setGender(employee.getGender() == null ? updateEmployee.getGender() : employee.getGender());
        updateEmployee.setAge(employee.getAge() == null ? updateEmployee.getAge() : employee.getAge());
        updateEmployee.setSalary(employee.getSalary() == null ? updateEmployee.getSalary() : employee.getSalary());
    }


    public void deleteEmployeeById(Integer id) {
        Employee deleteEmployee = employees.stream()
                .filter(employeeItem -> employeeItem.getId().equals(id))
                .findFirst()
                .orElse(null);
        employees.remove(deleteEmployee);
    }

    public List<Employee> getByPageAndSize(Integer page,Integer size){
        return employees.stream()
                .skip((page-1)*size)
                .limit(size)
                .collect(Collectors.toList());
    }
}
